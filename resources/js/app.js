import router from './router';
import store from './store';
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import axios from 'axios';
import App from './App.vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

require('./bootstrap');
window.Vue = require('vue').default;

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.component('contacts-component', require('./components/ContactsComponent').default);
Vue.component('header-component', require('./components/HeaderComponent').default);

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.getters.loggedIn) {
            next({
                name: 'login',
            })
        } else {
            next()
        }
    } else {
        next()
    }
});

const app = new Vue({
    el: '#app',
    components: {App},
    router,
    store
});


