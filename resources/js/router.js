import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginComponent from './components/LoginComponent'
import HomeComponent from "./components/HomeComponent";
import LogoutComponent from "./components/LogoutComponent";
import RegisterComponent from "./components/RegisterComponent";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'is-active',
    routes: [
        {
            name: 'login',
            path: '/login',
            component: LoginComponent
        },
        {
            name: 'home',
            path: '/',
            component: HomeComponent,
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/logout',
            name: 'logout',
            component: LogoutComponent
        },
        {
            path: '/register',
            name: 'register',
            component: RegisterComponent
        },
    ],
});

export default router