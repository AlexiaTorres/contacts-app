import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token') || null,
        name: localStorage.getItem('name') || null,
    },
    getters: {
        loggedIn(state) {
            return state.token !== null
        },
        name(state) {
            return state.name !== null
        },
    },
    mutations: {
        retrieveData(state, data) {
            state.token = data.token;
            state.name = data.name;
        },
        destroyToken(state) {
            state.token = null;
        },
        destroyUser(state) {
            state.name = null;
        },
    },
    actions: {
        retrieveData(context, credentials) {

            return new Promise((resolve, reject) => {
                axios.post('/api/auth/login', {
                    email: credentials.email,
                    password: credentials.password,
                })
                    .then(response => {
                        const token = response.data.access_token;
                        const name = response.data.user.name;
                        const data = {
                            'token': token,
                            'name': name,
                        };
                        localStorage.setItem('access_token', token);
                        localStorage.setItem('name', name);
                        context.commit('retrieveData', data);
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })

        },
        destroyToken(context) {
            if (context.getters.loggedIn) {
                localStorage.removeItem('access_token');
            }
        },
        destroyUser(context) {
            if (context.getters.name) {
                localStorage.removeItem('name');
            }
        },
    }
});

export default store