<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ContactCollection;
use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Resources\Contact as ContactResource;


class ContactsController extends Controller
{

    public function index()
    {
        return response()->json(new ContactCollection(Contact::all()));
    }

    public function store(Request $request)
    {
        $contact = new Contact();
        $contact->name = $request->name;
        $contact->phone = $request->phone;
        $contact->save();

        return new ContactResource($contact);
    }

    public function show($id)
    {
        return new ContactResource(Contact::FindOrFail($id));
    }

    public function update(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $contact->name = $request->name;
        $contact->phone = $request->phone;
        $contact->save();

        return new ContactResource($contact);
    }

    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();

        return new ContactResource($contact);
    }

    public function getDeletedContacts()
    {
        $contacts = Contact::onlyTrashed()->get();
        return response()->json($contacts);
    }

    public function recoverDeletedContacts($id)
    {
        Contact::withTrashed()->where('id',$id)->update(['deleted_at'=>null]);
    }
}
