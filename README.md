# Agenda de contactos

### Instalación
 - Crear la base de datos vacía en local

 - Copiar el contenido del .env.example en un archivo nuevo .env y setear alli los datos de la bbdd
    ```
    DB_DATABASE=xxx
    DB_USERNAME=xxx
    DB_PASSWORD=xxx
    ```
    ``` 
    composer install
    ```
    ```
    php artisan migrate 
    ```

    ```
    npm install
    ```
    ```
    npm run dev
    ```
    ```
    php artisan serve
    ```

   Si se van a hacer cambios en el mismo proyecto mejor tener en una pestaña lo de artisan y en otra ejecutado npm run watch
    
