<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', 'App\Http\Controllers\Auth\AuthController@login');
    Route::post('register', 'App\Http\Controllers\Auth\AuthController@register');
    Route::get('logout', 'App\Http\Controllers\Auth\AuthController@logout');
    Route::post('refresh', 'App\Http\Controllers\Auth\AuthController@refresh');
    Route::post('me', 'App\Http\Controllers\Auth\AuthController@me');
});

Route::apiResource('contacts','App\Http\Controllers\Api\ContactsController');
Route::get('deleted-contacts', 'App\Http\Controllers\Api\ContactsController@getDeletedContacts');
Route::get('recover-deleted-contacts/{id}', 'App\Http\Controllers\Api\ContactsController@recoverDeletedContacts');